<h1 align="center"><width="30px"> Lavalink Status <width="30px"></h1>

  ## Note
  - This repo is currently archived and not will be maintained until Discord.js 14 officially released. This doesn't mean you can't use this repo, you can still use it but if you counter with any error it will not be fixed.

  ## FAQ
  - Q: Can i use this repo in replit? A: Yes you can use it in replit but you need to update to Node.js version 17 by yourself to make this repo works.

  ## Requirements
  - Discord.js Version 14
  - Node.js Version 17+
  
  ## Installation
  Clone Repo
```
git clone https://github.com/LewdHuTao/Lavalink-Status.git
```
  After that run
  ```
npm i // To install all require packages
```
  Start bot with
```
node . or node index.js
```
